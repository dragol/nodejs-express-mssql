# IRONBUZZ

# Overview

IRONBUZZ project is dedicated to calculate phone call price depending of duration and to store information into database. The arhitecture consists of four servers:

 - Telephony Server
 - Process Server
 - IB Server
 - Database Server
 
# Requirements

 - NodeJS & NPM
 - Docker
 
# Installation
 - Clone the git repository
 - ***Telephony Server***

      Please, execute the following commands in your terminal:
 
      Navigate into the TelephonyServer directory:
	  
    `$ cd pathToClonedGitRepo/IronBuzz/TelephonyServer`
    	  
    Set environment variables:
    	 
    `$ cp .env.example .env`
     
    Install dependencies:
	
    `$ npm install`
	
    Finally, run the Telephony Server
	
    `$ npm run start`

    API documentation:
   
    To generate API documentation run next command:
   
    `$ npm run apidoc`

    Documentation files will be generated inside /apidoc directory.

 - ***Database Server***

    In order to setup MS SQL database server we will use Docker, please execute the following command:
	
    `$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d microsoft/mssql-server-linux:latest`
	  
    The command above will create Docker container with necessary database server.
    
    Seting up database and tables can be done in two ways.

    1.)
    
    `$ cd pathToClonedGitRepo/IronBuzz/TelephonyServer`
    
    And execute following commands in the terminal:
    
    `$ npm run db:migrate`
    
    `$ npm run db:seed`
    
    2.) 
    
    After the terminal command is executed, container id will appear in the terminal, please copy that value since we will need it in the next step.

    Now, run the following command in order to attach to container:
   
    `$ sudo docker exec -i -t ContainerIdCopiedInPreviousStep /bin/bash`

    Create empty file:
   
    `$ touch sql.sql`
   
    Now, copy content of mssql_database.sql file located in the root of IRONBUZZ project, and paste value inside quotes in next command:
    
    `$ echo 'CopiedValueFrom musql_database.sql File' > sql.sql`
    
    Next step is to connect to MS SQL server, please, run the following terminal command:

    `$ /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'yourStrong(!)Password'`

    And finally, create database and tables with data by running this command:
   
    `$ /opt/mssql-tools/bin/sqlcmd  -S localhost -U SA -P 'yourStrong(!)Password' -i a.sql`

 - ***Process Server***

    Please, execute the following commands in your terminal:
 
    Navigate into the ProcessServer directory:
	  
    `$ cd pathToClonedGitRepo/IronBuzz/ProcessServer`

    Set environment variables:
	 
    `$ cp .env.example .env`
	  
    Install dependencies:
	
    `$ npm install`

    Finally, run the Process Server

    `$ npm run start`
	  
 - ***IB Server***

    Please, execute the following commands in your terminal:
 
    Navigate into the IBServer directory:
	  
    `$ cd pathToClonedGitRepo/IronBuzz/IBServer`
	  
    Set environment variables:
	 
    `$ cp .env.example .env`
	 
    Install dependencies:
	
    `$ npm install`
	  
    Finally, run the IB Server
	
    `$ npm run start`
	 
That is all, go nuts :)
