module.exports = function (data) {
  const jsonArray = [];
  data.forEach((columns) => {
    const rowObject = {};
    columns.forEach((column) => {
      rowObject[column.metadata.colName] = column.value;
    });
    jsonArray.push(rowObject);
  });
  return jsonArray;
};
