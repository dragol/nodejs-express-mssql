const TediousPromises = require('../database/db-connection');
const TYPES = require('tedious').TYPES;

const IbCallModel = {
  getNewCalls() {
    const sqlQuery = `SELECT 
                                call_id, 
                                duration 
                            FROM 
                                ib_call_table 
                            WHERE 
                                calculated_price IS NULL 
                                AND status IS NULL`;

    return TediousPromises.sql(sqlQuery).execute();
  },

  createCall(data) {
    const sqlQuery = `INSERT INTO ib_call_table
                                  (
                                   call_id, duration, status, calculated_price
                                  ) 
                                  VALUES 
                                  (@callId, @duration, NULL, NULL)`;

    return TediousPromises.sql(sqlQuery)
      .parameter('callId', TYPES.Int, data.call_id)
      .parameter('duration', TYPES.VarChar, data.duration)
      .execute();
  },
};

module.exports = IbCallModel;
