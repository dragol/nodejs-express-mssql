'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('ib_call_rate', [
          {
            duration_from: '1',
            duration_to: '3',
            rate: 2
          },
          {
            duration_from: '4',
            duration_to: '6',
            rate: 1.5
          },
          {
            duration_from: '7',
            duration_to: '9',
            rate: 0.8
          },
          {
            duration_from: '10',
            duration_to: '1000',
            rate: 0.3
          }], {});
      },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ib_call_rate', null, {});
  }
};
