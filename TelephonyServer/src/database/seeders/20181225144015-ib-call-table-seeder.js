'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('ib_call_table', [
          {
            call_id: '1',
            duration: '9'
          },
          {
            call_id: '2',
            duration: '10'
          },
          {
            call_id: '3',
            duration: '10'
          }], {});
      },

      down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('ib_call_table', null, {});
      }
};
