'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ib_call_rate', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        duration_from: {
          type: Sequelize.STRING
        },
        duration_to: {
          type: Sequelize.STRING
        },
        rate: {
          allowNull: true,
          type: Sequelize.DECIMAL(10, 1)
        }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ib_call_rate');
  }
};
