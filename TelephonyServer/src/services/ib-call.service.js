const axios = require('axios');
const ibCallService = require('../services/ib-call.service');
const ibCallModel = require('../models/ib-call.model');
const successResponse = require('../utils/response-handler').successResponse;
const errorResponse = require('../utils/response-handler').errorResponse;


const sendDataToProcessServer = (results, callbackUrl) => {
  try {
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    results.forEach(data => axios.post(callbackUrl, {
      err: '',
      data,
    },
    axiosConfig));
  } catch (error) {
    console.error(error);
  }
};

exports.getNewCallsAndSendDataToProcessServer = (res, callbackUrl) => {
  ibCallModel.getNewCalls()
    .then((results) => {
      sendDataToProcessServer(results, callbackUrl);
      successResponse(res, results);
    }).fail((err) => {
      errorResponse(res, err);
    });
};
