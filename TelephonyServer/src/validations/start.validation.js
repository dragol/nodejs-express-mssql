const Joi = require('joi');

const schema = Joi.object().keys({

  url: Joi.string().uri().trim().required(),

});

module.exports = schema;
