const Joi = require('joi');

const schema = Joi.object().keys({
  err: Joi.string().allow('').optional(),
  data: Joi.object().keys({
    call_id: Joi.number().integer().required(),
    duration: Joi.string().required(),
  })
});

module.exports = schema;
