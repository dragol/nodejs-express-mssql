const ibCallService = require('../services/ib-call.service');
const ibCallModel = require('../models/ib-call.model');
const STATUS = require('../statuses');
const successResponse = require('../utils/response-handler').successResponse;
const errorResponse = require('../utils/response-handler').errorResponse;

let curentStatus = STATUS.EMPTY;

exports.start = (req, res) => {
  if (curentStatus === STATUS.EMPTY) {
    curentStatus = STATUS.IN_PROGRESS;
    ibCallService.getNewCallsAndSendDataToProcessServer(res, req.body.url);
  } else {
    errorResponse(res, { message: 'Server is already started.' });
  }
};

exports.stop = (req, res) => {
  if (curentStatus === STATUS.IN_PROGRESS) {
    curentStatus = STATUS.EMPTY;
    successResponse(res, { message: 'Status is now stopped.' });
  } else {
    errorResponse(res, { message: 'Can not stop server, status is not in progress.' });
  }
};

exports.pause = (req, res) => {
  if (curentStatus === STATUS.IN_PROGRESS) {
    curentStatus = STATUS.PAUSED;
    successResponse(res, { message: 'Status is now paused.' });
  } else {
    errorResponse(res, { message: 'Can not pause server, status is not in progress.' });
  }
};

exports.resume = (req, res) => {
  if (curentStatus === STATUS.PAUSED) {
    curentStatus = STATUS.IN_PROGRESS;
    ibCallService.getNewCallsAndSendDataToProcessServer(res, req.body.url);
  } else {
    errorResponse(res, { message: 'Can not resume server, status is not paused.' });
  }
};

exports.createCall = (req, res) => {
  ibCallModel.createCall(req.body.data)
    .then((results) => {
      successResponse(res, { message: 'Call successfully saved.' });
    }).fail((err) => {
      errorResponse(res, err);
    });
};
