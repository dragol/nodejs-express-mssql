const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');
require('dotenv').config();

const routes = require('./routes/routes');

const app = express();
const PORT = process.env.TELEPHONY_SERVER_PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet());
app.use(compression());

routes(app);

app.listen(PORT, () => {
  console.log(`TelephonyServer is running on PORT: ${PORT}`);
});
