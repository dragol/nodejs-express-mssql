module.exports = function (app) {
  const Joi = require('joi');
  const validator = require('express-joi-validation')({
    passError: true,
  });

  const startRouteRequestValidationSchema = require('../validations/start.validation');
  const createCallRouteRequestValidationSchema = require('../validations/call-start.validation');
  const ibCallController = require('../controllers/ib-call.controller');

  // server routes
  /**
* @api {post} /start List all unprocessed calls and initialize telephony server
* @apiDescription Get a list of all unprocessed calls
* @apiVersion 1.0.0
* @apiName start
* @apiGroup IbCalls
*
* @apiHeader {String} Content-Type: application/json
*
* @apiParam  {String}  url  url of process server
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*         "data": [
*              {
*                 "call_id": 1,
*                 "duration": "9"
*              }
*         ]
*      }
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": {
*             "message": "Server is already started."
*          },
*         "data": ""
*     }
*
* @apiError (UnprocessableEntity 422)  UnprocessableEntity Json payload is in incorrect format
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 422 Unprocessable Entity
*     {
*          "err": "Invalid request data.",
*          "data": ""
*     }
*/
  app.post('/start', validator.body(startRouteRequestValidationSchema), (req, res, next) => {
    ibCallController.start(req, res);
  });

  /**
* @api {post} /stop Stop the telephony server
* @apiDescription Stop the telephony server
* @apiVersion 1.0.0
* @apiName stop
* @apiGroup IbCalls
*
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*         "data": {
*            "message": "Status is now stopped."
*            }
*      }
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": {
*              "message": "Can not stop server, status is not in progress."
*          },
*          "data": ""
*     }
*/
  app.route('/stop')
    .post(ibCallController.stop);

  /**
* @api {post} /resume Resume the telephony server status
* @apiDescription Resume the telephony server status
* @apiVersion 1.0.0
* @apiName resume
* @apiGroup IbCalls
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*         "data": [
*              {
*                 "call_id": 1,
*                 "duration": "9"
*              }
*         ]
*      }
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": {
*              "message": "Can not resume server, status is not paused."
*          },
*          "data": ""
*     }
*/
  app.route('/resume')
    .post(ibCallController.resume);

  /**
* @api {post} /pause Pause the telephony server status
* @apiDescription Pause the telephony server status
* @apiVersion 1.0.0
* @apiName pause
* @apiGroup IbCalls
*
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*          "err": "",
*          "data": {
*             "message": "Status is now paused."
*          }
*     }
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 200 OK
*     {
*          "err": {
*              "message": "Can not pause server, status is not in progress."
*           },
*           "data": ""
*     }
*/
  app.route('/pause')
    .post(ibCallController.pause);


  /**
* @api {post} /call/create Create new call
* @apiDescription Create new call
* @apiVersion 1.0.0
* @apiName callCreate
* @apiGroup IbCalls
*
* @apiHeader {String} Content-Type: application/json
*
* @apiParam  {String}  call_id  id of the call
* @apiParam  {String}  duration  duration of the call
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*         "data":
*              {
*                 "message": "Call successfully saved.",
*              }
*      }
*
* @apiError (UnprocessableEntity 422)  UnprocessableEntity Json payload is in incorrect format
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 422 Unprocessable Entity
*     {
*          "err": "Invalid request data.",
*          "data": ""
*     }
*/
  app.post('/call/create', validator.body(createCallRouteRequestValidationSchema), (req, res, next) => {
    ibCallController.createCall(req, res);
  });

  // joi
  app.use((err, req, res, next) => {
    if (err.error.isJoi) {
      res.status(422).send({
        err: 'Invalid request data.',
        data: '',
      });
    } else {
      next(err);
    }
  });

  // 404
  app.use((req, res) => res.status(404).send({ err: { message: `Route ${req.url} is not found.` }, data: '' }));

  // error handlers
  if (app.get('env') === 'development') {
    app.use((err, req, res) => res.status(500).send({ err, data: { message: err.message } }));
  }

  if (app.get('env') === 'production') {
    app.use((err, req, res) => res.status(500).send({ err: { message: 'It\'s our fault, we messed something up.' }, data: '' }));
  }
};
