module.exports = function (app) {
    const Joi = require('joi');
    const validator = require('express-joi-validation')({
      passError: true,
    });
  
  const mainRouteValidationSchema = require('../validations/process.validation');
  const processController = require('../controllers/process.controller');

  // server routes
  /**
* @api {post} / Process server entry point
* @apiDescription Recieve call id and duration
* @apiVersion 1.0.0
* @apiName /
* @apiGroup ProcessServer
*
* @apiHeader {String} Content-Type: application/json
*
* @apiParam  {String}  call_id  id of the call
* @apiParam  {String}  duration  duration of the call
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*          "data": [
*           {
*            "rate": 1.5
*            }
*           ]
*      }
*
* @apiError (UnprocessableEntity 422)  UnprocessableEntity Json payload is in incorrect format
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 422 Unprocessable Entity
*     {
*          "err": "Invalid request data.",
*          "data": ""
*     }
*/
  app.post('/', validator.body(mainRouteValidationSchema), (req, res, next) => {
    processController.process(req, res);
  });

  // joi
  app.use((err, req, res, next) => {
    if (err.error.isJoi) {
      res.status(422).send({
        err: 'Invalid request data.',
        data: '',
      });
    } else {
      next(err);
    }
  });

    // 404
    app.use((req, res) => res.status(404).send({ err: { message: `Route ${req.url} is not found.` }, data: '' }));

    // error handlers
    if (app.get('env') === 'development') {
      app.use((err, req, res) => res.status(500).send({ err, data: { message: err.message } }));
    }
  
    if (app.get('env') === 'production') {
      app.use((err, req, res) => res.status(500).send({ err: { message: 'It\'s our fault, we messed something up.' }, data: '' }));
    }
};
