const processService = require('../services/process.service');
const ibCallRateModel = require('../models/ib-call-rate.model');
const successResponse = require('../utils/response-handler').successResponse;
const errorResponse = require('../utils/response-handler').errorResponse;

exports.process = function (req, res) {
     const callId = req.body.data.call_id;
     const callDuration = req.body.data.duration;
    ibCallRateModel.getRateForCallDuration(callDuration)
    .then((results) => {
        processService.sendDataToIBServer(callId, callDuration, results[0].rate);
        successResponse(res, results);
    }).fail((err) => {
        errorResponse(res, err);
    });
};
