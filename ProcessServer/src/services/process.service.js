const axios = require('axios');

exports.sendDataToIBServer = async (callId, callDuration, rate) => {
    try {
        const data = { call_id: callId, calculated_price: `${callDuration * rate}` };
        const axiosConfig = {
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const uri = `${process.env.IB_SERVER}:${process.env.IB_SERVER_PORT}/consume`;
          await axios.post(uri, {
            err: '',
            data
          },
          axiosConfig)
    } catch (error) {
        console.error(error);
    }
};
