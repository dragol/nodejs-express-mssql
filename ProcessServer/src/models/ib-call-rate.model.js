const TediousPromises = require('../database/db-connection');
const TYPES = require('tedious').TYPES;

const IbCallRateModel = {
  getRateForCallDuration(callDuration) {
   
      const sqlQuery = `SELECT 
                            rate 
                        FROM 
                            ib_call_rate 
                        WHERE 
                            @callDuration BETWEEN duration_from 
                            AND duration_to`;

       return TediousPromises.sql(sqlQuery)
       .parameter('callDuration', TYPES.VarChar, callDuration)
       .execute()
  },
};

module.exports = IbCallRateModel;
