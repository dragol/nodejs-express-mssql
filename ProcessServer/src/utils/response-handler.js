module.exports = {
    successResponse: (res, data) => {
        res.send({ err: '', data: data });
    },

    errorResponse: (res, err) => {
        res.send({ err: err, data: '' });
    }
};