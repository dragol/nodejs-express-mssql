CREATE DATABASE ib;
GO
USE ib;

CREATE TABLE dbo.ib_call_rate (
	id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	duration_from varchar(max) NULL,
	duration_to varchar(max) NULL,
	rate decimal(18,1) NULL
);

-- Add 4 rows for ib_call_rate.
SET IDENTITY_INSERT ib_call_rate ON
INSERT INTO ib_call_rate (id, duration_from, duration_to, rate) VALUES 
(1,'1','3',2),
(2,'4','6',1.5),
(3,'7','9',0.8),
(4,'10','1000',0.3);

CREATE TABLE dbo.ib_call_table (
	id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	call_id int NULL,
	duration varchar(max) NULL,
	status varchar(max) NULL,
	calculated_price varchar(max) NULL
);

-- Add 3 rows for ib_call_table.
SET IDENTITY_INSERT ib_call_rate ON
INSERT INTO ib_call_table (call_id, duration, status, calculated_price) VALUES 
(1,'9',NULL,NULL),
(2,'10',NULL,NULL),
(3,'10',NULL,NULL);

