const TediousPromises = require('../database/db-connection');
const TYPES = require('tedious').TYPES;
const STATUSES = require('../statuses');

const IbCallRateModel = {
  updateCalculatedPriceAndStatus(data) {
    const sqlQuery = `UPDATE 
                            ib_call_table 
                        SET 
                            status = '${STATUSES.DONE}', 
                            calculated_price = @calculatedPrice 
                        WHERE 
                            call_id = @callId`;

    return TediousPromises.sql(sqlQuery)
      .parameter('calculatedPrice', TYPES.VarChar, data.calculated_price)
      .parameter('callId', TYPES.Int, data.call_id)
      .execute();
  },
};

module.exports = IbCallRateModel;
