const ibCallRateModel = require('../models/ib-call-rate.model');
const successResponse = require('../utils/response-handler').successResponse;
const errorResponse = require('../utils/response-handler').errorResponse;

exports.updateCalculatedPriceAndStatus = function (req, res) {
  ibCallRateModel.updateCalculatedPriceAndStatus(req.body.data)
    .then((results) => {
      successResponse(res, { massage: 'Price successfuly updated.' });
    }).fail((err) => {
      errorResponse(res, err);
    });
};
