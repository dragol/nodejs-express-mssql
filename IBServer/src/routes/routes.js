module.exports = function (app) {
  const validator = require('express-joi-validation')({
    passError: true,
  });

  const consumeRouteValidationSchema = require('../validations/consume.validation');
  const ibController = require('../controllers/ib.controller');

  // server routes
  /**
* @api {post} /consume Update calculated price
* @apiDescription Recieve calculated price and call id after what updates database
* @apiVersion 1.0.0
* @apiName /
* @apiGroup IBServer
*
* @apiHeader {String} Content-Type: application/json
*
* @apiParam  {String}  call_id  id of the call
* @apiParam  {String}  calculated_price  calculated price the call
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*         "err": "",
*          "data":
*           {
*            "message": "Price successfuly updated."
*            }
*      }
*
* @apiError (UnprocessableEntity 422)  UnprocessableEntity Json payload is in incorrect format
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 422 Unprocessable Entity
*     {
*          "err": "Invalid request data.",
*          "data": ""
*     }
*/
  app.post('/consume', validator.body(consumeRouteValidationSchema), (req, res, next) => {
    ibController.updateCalculatedPriceAndStatus(req, res);
  });

  // joi
  app.use((err, req, res, next) => {
    if (err.error.isJoi) {
      res.status(422).send({
        err: 'Invalid request data.',
        data: '',
      });
    } else {
      next(err);
    }
  });

  // 404
  app.use((req, res) => res.status(404).send({ err: { message: `Route ${req.url} is not found.` }, data: '' }));

  // error handlers
  if (app.get('env') === 'development') {
    app.use((err, req, res) => res.status(500).send({ err, data: { message: err.message } }));
  }

  if (app.get('env') === 'production') {
    app.use((err, req, res) => res.status(500).send({ err: { message: 'It\'s our fault, we messed something up.' }, data: '' }));
  }
};
