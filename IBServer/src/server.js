const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();

const routes = require('./routes/routes');

const app = express();
const PORT = process.env.PROCESS_SERVER_PORT || 3002;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes(app);

app.listen(PORT, () => {
  console.log(`IBServer is running on PORT: ${PORT}`);
});
