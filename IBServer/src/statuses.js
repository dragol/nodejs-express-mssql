const IN_PROGRESS = 'in progress';
const PAUSED = 'paused';
const EMPTY = '';
const DONE = 'done';

module.exports = {
  IN_PROGRESS,
  PAUSED,
  EMPTY,
  DONE
};
