const ConnectionPool = require('tedious-connection-pool');
const TediousPromises = require('tedious-promises');

const dbConfig = {
  userName: process.env.DB_USER,
  password: process.env.DB_PASS,
  server: process.env.DB_HOST,
  port: process.env.DB_HOST_PORT,
  options: {
    database: process.env.DB_NAME,
    encrypt: true,
    rowCollectionOnRequestCompletion: true,
  },
};

const poolConfig = {
  min: 2,
  max: 8,
  log: false,
};

const pool = new ConnectionPool(poolConfig, dbConfig);
TediousPromises.setConnectionPool(pool);

module.exports = TediousPromises;
