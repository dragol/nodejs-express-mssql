module.exports = {
  successResponse: (res, data) => {
    res.send({ err: '', data });
  },

  errorResponse: (res, err) => {
    res.send({ err, data: '' });
  },
};
